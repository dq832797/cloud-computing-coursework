import pandas as pd
import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import matplotlib.pyplot as plt; plt.rcdefaults()
import matplotlib.pyplot as plt
import time

def time_conv(tim):
    """
    convert unix epoch time to date time
    """
    tim_temp=[]
    tim_out=[]
    N,=tim.shape
    for i in range(len(tim)):
        tim_temp.extend(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(tim[i])))
        tim_temp=[''.join(tim_temp[12:20])]
        tim_out.extend(tim_temp)
    return(tim_out)

def haversine_distance(lat1, lon1, lat2, lon2):
    r = 6371
    phi1 = np.radians(lat1)
    phi2 = np.radians(lat2)
    delta_phi = np.radians(lat2 - lat1)
    delta_lambda = np.radians(lon2 - lon1)
    a = np.sin(delta_phi / 2)**2 + np.cos(phi1)*np.cos(phi2)*np.sin(delta_lambda / 2)**2
    res = r * (2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a)))
    return np.round(res, 2)

def airport_distance(df_airports, dep_air, arr_air):
    """
    Takes departure and arrival airports and calculates the distance between them
    using the reference input df_airports.
    """
    N,=dep_air.shape
    N1,_=df_airports.shape
    dist=pd.DataFrame()

    long_lat=pd.DataFrame(columns=['distance','lattitude_dep','longtitude_dep','dep_name',
                                   'lattitude_arr','longtitude_arr','arr_name'])
    R = 6373.0 #radius of earth
    for i in range(N):
        for j in range(N1):
            if dep_air[i] == df_airports.iloc[j,1]:
                long_lat.loc[i,'lattitude_dep']=df_airports.iloc[j,2]#lattitude departure
                long_lat['longtitude_dep'][i]=df_airports.iloc[j,3]#longtitude departure
                long_lat['dep_name'][i]= df_airports.iloc[j,0]# name of departure airport


            if arr_air[i] == df_airports.iloc[j,1]:
                long_lat.loc[i,'lattitude_arr']=df_airports.iloc[j,2] #lattitude arrival
                long_lat['longtitude_arr'][i]=df_airports.iloc[j,3] #longtitude arrival
                long_lat['arr_name'][i] = df_airports.iloc[j,0] #name of arrival airport

        long_lat['distance'][i]=haversine_distance(long_lat['lattitude_dep'][i], long_lat['longtitude_dep'][i],
                                                  long_lat.loc[i,'lattitude_arr'], long_lat['longtitude_arr'][i])

    return(long_lat)

def not_used(df_airports, compare):
    """Determines which airports have not been departed from"""
    df_out=pd.DataFrame(columns=['name','code','latt','long'])
    df_code=pd.DataFrame(columns=['val'])
    df_long=pd.DataFrame(columns=['val'])
    df_latt=pd.DataFrame(columns=['val'])
    pos=0
    for j in range(len(df_airports[1])):
        if df_airports[1][j] not in compare['key'].tolist():
            df_out.loc[pos,'name']=df_airports[0][j]
            df_out.loc[pos,'code']=df_airports[1][j]
            df_out.loc[pos,'latt']=df_airports[2][j]
            df_out.loc[pos,'long']=df_airports[3][j]
            pos=pos+1
    return(df_out)
#######################################################################################
#######################################################################################

def hover_out(df_flight_paths,flight_des,flight_val):
    """ adds aditional utility to the plotly library cabapility by adding more than one value 
        to a datapoint on hover
    """
    dep_time=time_conv(pd.to_numeric(flight_des[3])) # converting from unix epoch time
    arr_time=time_conv(pd.to_numeric(flight_des[3])+pd.to_numeric(flight_des[4])*60)
    hover_=""
    text_out=[]
    long=pd.DataFrame(columns=[0, 1])
    latt=pd.DataFrame(columns=[0, 1])
    for iter in range(len(flight_des)):
        hover_=""
        for j in range(len(flight_des)):
            if flight_des[1][iter]==flight_des[1][j]: #generatring the required output format
                hover_temp="<br>"+"Flight no: "+flight_des[0][j]+"<br>"+"Depart Airport: "+\
                    df_flight_paths['dep_name'][j]+"-"+flight_des[1][j]+"<br>"+"Departure time:"+dep_time[j]+"<br>"+"Arrival Airport: "+\
                    df_flight_paths['arr_name'][j]+"-"+flight_des[2][j]+"<br>"  +"Arrival Time:"+arr_time[j]+"<br>"+"Flight Times-"+\
                    flight_des[4][j] + "<br>" + "Passenger No:" + flight_val[j]
                hover_ = hover_ + hover_temp
        hover_=[''.join(hover_[0:2000])]#adds limit to the number of strings on graphic
        text_out.extend(hover_)
        long[iter] =df_flight_paths['longtitude_dep']
        latt[iter] =df_flight_paths['lattitude_dep']

    return(text_out, long[iter],latt[iter])

#######################################################################################
#######################################################################################

class Infographics(object):
    """
    Class to generate infographics
    """
    def info_out(df_airports,to_sort_ob1,to_sort_ob2,to_sort_ob3,to_sort_ob4):
        to_sort_ob1.reset_index(level=0, inplace=True) #reseting the index
        to_sort_ob2.reset_index(level=0, inplace=True) #reseting the index
        to_sort_ob3.reset_index(level=0, inplace=True) #eseting the index
        to_sort_ob4.reset_index(level=0, inplace=True)

        fig = go.Figure()
        ######################### Creating the subplots##########################
        fig = make_subplots(
            rows=3, cols=2,
            column_widths=[0.6,0.4],
            row_heights=[0.3, 0.3,0.3],
            subplot_titles=('Objective 2 -Departure and Objective 1- not used Airports', 'Objective 1 -Number of Flights From Each Airport',
                           'Objective 3 -Number of passengers on Each Flight','Objective 4 -Total Distance Travelled By Each Passenger'),
            specs=[[{"type": "scattergeo", "rowspan": 3}, {"type": "bar"}],
                  [            None                    , {"type": "bar"}],
                  [            None                    , {"type": "bar"}]])

        #########################################################################

        flight_des = to_sort_ob2['key'].apply(lambda x: pd.Series([i for i in x.split('@,')]))
        flight_val =  to_sort_ob2['value']
        df_flight_paths=airport_distance(df_airports, flight_des[1],flight_des[2])
        hover_,long,latt=hover_out(df_flight_paths,flight_des,flight_val) #associating hover text to data points
        fig.add_trace(go.Scattergeo(
            locationmode = 'USA-states',
            showlegend=False,
            lon = long, #longtitude
            lat = latt, #lattitude
            hoverinfo = 'text',
            text = hover_,
            mode = 'markers',
            marker = dict(
                size = 8,
                color = 'rgb(0, 0, 0)',
                line = dict(
                    width = 3,
                    color = 'rgba(68, 68, 68, 0)'
                )
            )), row=1, col=1)

        not_air=not_used(df_airports, to_sort_ob1) # Calculating which airports have not been used

        fig.add_trace(go.Scattergeo(
            locationmode = 'USA-states',
            showlegend=False,
            lon = not_air['long'], #longtitude
            lat = not_air['latt'], #lattitude
            hoverinfo = 'text',
            text = not_air['name']+"-"+not_air['code']+"-Not Used",
            mode = 'markers',
            marker = dict(
                size = 8,
                color = 'rgb(255, 102, 178)',
                line = dict(
                    width = 3,
                    color = 'rgba(68, 68, 68, 0)'
                )
            )), row=1, col=1)

        fig.add_trace(
            go.Bar(x=to_sort_ob1["key"],y=to_sort_ob1["value"], marker=dict(color="crimson"), showlegend=False
                ),
            row=1, col=2
        )

        fig.add_trace(
            go.Bar(x=to_sort_ob3["key"],y=to_sort_ob3["value"], marker=dict(color="crimson"), showlegend=False, 
                ),
            row=2, col=2
        )

        fig.add_trace(
            go.Bar(x=to_sort_ob4["key"],y=to_sort_ob4["value"], marker=dict(color="crimson"), showlegend=False, 
                ),
            row=3, col=2
        )


        fig['layout']['yaxis']['title']='Total number of Flights'
        fig['layout']['yaxis2']['title']='Total Number of Passengers'
        fig['layout']['yaxis3']['title']='Total distance in Km'

        fig.update_layout(title_text="Airport departure results. Move your mouse over the airport locations to see passengers on each flight")

        fig.show()