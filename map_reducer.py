from error_check import Error_check as er # internal
import pandas as pd
import numpy as np
import os

def key_val_map(key, value,val_const=True):

    """ returns a dataframe of key val pairs if val_const True 
        then a consntant value is returned
    """

    if val_const:
        N,=key.shape
        s = pd.Series([value],name='value')
        value= s.repeat(N).reset_index(drop=True)
        return(pd.concat([key, value], axis=1))
    else:
        return(pd.concat([key, value], axis=1))
        
def combiner(key_val):
    """ 
    Requires a series dataframe input returns sum of value and key paris based on the key
    """
    key_val.columns=["key","value"]
    #print(key_val)
    return(key_val.groupby(by=['key']).sum())

def haversine_distance(lat1, lon1, lat2, lon2):
    r = 6371
    phi1 = np.radians(lat1)
    phi2 = np.radians(lat2)
    delta_phi = np.radians(lat2 - lat1)
    delta_lambda = np.radians(lon2 - lon1)
    a = np.sin(delta_phi / 2)**2 + np.cos(phi1)*np.cos(phi2)*np.sin(delta_lambda / 2)**2
    res = r * (2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a)))
    return np.round(res, 2)

def airport_distance(df_airports, dep_air, arr_air):
    """
    Takes departure and arrival airports and calculates the distance between them
    using the reference input df_airports.
    """
    N,=dep_air.shape
    N1,_=df_airports.shape
    dist=pd.DataFrame()

    long_lat=pd.DataFrame(columns=['distance','lattitude_dep','longtitude_dep','dep_name',
                                   'lattitude_arr','longtitude_arr','arr_name'])
    R = 6373.0 #radius of earth
    for i in range(N):
        for j in range(N1):
            if dep_air[i] == df_airports.iloc[j,1]:
                long_lat.loc[i,'lattitude_dep']=df_airports.iloc[j,2]#lattitude departure
                long_lat['longtitude_dep'][i]=df_airports.iloc[j,3]#longtitude departure
                long_lat['dep_name'][i]= df_airports.iloc[j,0]# name of departure airport


            if arr_air[i] == df_airports.iloc[j,1]:
                long_lat.loc[i,'lattitude_arr']=df_airports.iloc[j,2] #lattitude arrival
                long_lat['longtitude_arr'][i]=df_airports.iloc[j,3] #longtitude arrival
                long_lat['arr_name'][i] = df_airports.iloc[j,0] #name of arrival airport

        long_lat['distance'][i]=haversine_distance(long_lat['lattitude_dep'][i], long_lat['longtitude_dep'][i],
                                                  long_lat.loc[i,'lattitude_arr'], long_lat['longtitude_arr'][i])

    return(long_lat)

class Map_reducer(object):
    """
    Class that contains the map and reducer chain jobs
    """

    def run_mapper(df_airports, thread_id,airports,input_data):
        """ mapper and local reducer are combined """
        ############ Cleaning the data  and seperating into keys ####################
        input_data=input_data.reset_index(drop=True)
        df_pass, df_flights, df_dep_tim,df_flight_time = er.data_cleaner(input_data,airports)### cleaning data
        long_lat=airport_distance(df_airports, df_flights[1],df_flights[2]) ## caclulating distance

        ########################### obj1 ############################

        count_ob1=key_val_map(df_flights[1],1,val_const=True) ### map
        count_ob1=combiner(count_ob1) ### combine
        count_ob1.to_csv(os.getcwd() + r"\data_out\map-" +r"ob1_" + str(thread_id) + r".ob1", header=False) ##out

        ########################### obj2 ############################
        key_ob2=df_flights.iloc[:,0]+"@,"+df_flights[1]+"@,"+df_flights[2]+"@,"+df_dep_tim.astype(str)\
            +"@,"+df_flight_time.astype(str) ## creating key for flights
        count_ob2=key_val_map(key_ob2, "<br>"+df_pass, val_const=False) 
        count_ob2=combiner(count_ob2) ### combine
        count_ob2.to_csv(os.getcwd() + r"\data_out\map-" +r"ob2_" + str(thread_id) + r".ob2", header=False) ##out

        ########################### obj3 ############################

        count_ob3=key_val_map(df_flights[0],1) ######## Map
        count_ob3=combiner(count_ob3) ####### Combine
        count_ob3.to_csv(os.getcwd() + r"\data_out\map-" +r"ob3_" + str(thread_id) + r".ob3", header=False) ##out

        ########################### obj4 ############################
    
        count_ob4= key_val_map(df_pass,long_lat['distance'], False)  ##### Mapping
        count_ob4 = combiner(count_ob4) ######## Combining
        count_ob4.to_csv(os.getcwd() + r"\data_out\map-" +r"ob4_" + str(thread_id) + r".ob4", header=False) ##out


        return(count_ob1, count_ob3, count_ob4)

    def reducer_combiner(threads):
        to_sort_ob1=pd.DataFrame(columns=[0, 1])
        to_sort_ob2=pd.DataFrame(columns=[0, 1])
        to_sort_ob3=pd.DataFrame(columns=[0, 1])
        to_sort_ob4=pd.DataFrame(columns=[0, 1])
        map_input=pd.DataFrame()
        for thread_id in range(threads): #pulling in map files
            ######### Ob1 #########
            map_input=pd.read_csv(os.getcwd() + r"\data_out\map-" +r"ob1_" + str(thread_id) + r".ob1", header=None) #
            to_sort_ob1=pd.concat([map_input,to_sort_ob1],axis=0, join="inner") #merging outputs
            os.remove(os.getcwd() + r"\data_out\map-" +r"ob1_" + str(thread_id) + r".ob1") #removing files to save space
        
            ######### Obj2 ########
            map_input=pd.read_csv(os.getcwd() + r"\data_out\map-" +r"ob2_" + str(thread_id) + r".ob2", header=None) #
            to_sort_ob2=pd.concat([map_input,to_sort_ob2],axis=0, join="inner") #merging outputs
            os.remove(os.getcwd() + r"\data_out\map-" +r"ob2_" + str(thread_id) + r".ob2") #removing files to save space

            ######### Ob3 #########
            map_input=pd.read_csv(os.getcwd() + r"\data_out\map-" +r"ob3_" + str(thread_id) + r".ob3", header=None) 
            to_sort_ob3=pd.concat([map_input,to_sort_ob3],axis=0, join="inner")
            os.remove(os.getcwd() + r"\data_out\map-" +r"ob3_" + str(thread_id) + r".ob3") #removing files to save space

            ######### Ob4 #########

            map_input=pd.read_csv(os.getcwd() + r"\data_out\map-" +r"ob4_" + str(thread_id) + r".ob4", header=None) 
            to_sort_ob4=pd.concat([map_input,to_sort_ob4],axis=0, join="inner")
            os.remove(os.getcwd() + r"\data_out\map-" +r"ob4_" + str(thread_id) + r".ob4") #removing files to save space

        to_sort_ob1=combiner(to_sort_ob1)
        to_sort_ob2=combiner(to_sort_ob2)
        to_sort_ob3=combiner(to_sort_ob3)
        to_sort_ob4=combiner(to_sort_ob4)
        #### Creating raw output files for additional external maniputlation #####
        to_sort_ob1.to_csv(os.getcwd() + r"\data_out\map-" +r"ob1_out" + r".csv", header=False)
        to_sort_ob2.to_csv(os.getcwd() + r"\data_out\map-" +r"ob2_out" + r".csv", header=False)
        to_sort_ob3.to_csv(os.getcwd() + r"\data_out\map-" +r"ob3_out" + r".csv", header=False)
        to_sort_ob4.to_csv(os.getcwd() + r"\data_out\map-" +r"ob4_out" + r".csv", header=False)

        ################# Need to add final error check ###############
        return(to_sort_ob1,to_sort_ob2,to_sort_ob3,to_sort_ob4)





