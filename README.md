# Cloud Computing Coursework

Project developed for the cloud computing module of an advanced computer science MSc

The following is a a simple MapReduce framework. There are four output files (in data_out) related to each specific objective.

* **obj1 Determine the number of flights from each airport; include a list of any airports not used.

* **obj2 Create a list of flights based on the Flight id, this output should include the passenger Id, relevant
  IATA/FAA codes, the departure time, the arrival time (times to be converted to HH:MM:SS format),
  and the flight times.

* **obj3 Calculate the number of passengers on each flight.

* **obj4 Calculate the line-of-sight (nautical) miles for each flight and the total travelled by each passenger
  and thus output the passenger having earned the highest air miles.

The data_out older contains the CSV output files related to each Objective

The output for the objectives has also been visualised. For each departure airport on the map a hover over exists that provides the output for objective 2
The unused airports are given by the pink dots

![Cloud_output](/data_out/output_visual.png)



For further information please email dq832797@student.reading.ac.uk or if I have graduated please email my personnel email fourthquantum@hotmail.co.uk