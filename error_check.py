import numpy as np
import Levenshtein
import pandas as pd


def clean_replace(df_merge,df_compare,lev,format=True):

    """ Requires one column dataframe to be cleaned and a format compare 
    along and index indicating formating errors
    lev is the levenshtein ratio between comparison list and input data
    """
   
    N,=df_merge.shape
    for index, value in df_compare.items():
        if value and format:
            pass
        else:
            for ind in range(N):
                if df_merge[index] == df_merge[ind]:
                    pass
                elif Levenshtein.ratio(df_merge[index],df_merge[ind]) >= lev: #finds the best match
                    #print(df_merge[index],df_merge[ind]) # checking it's working
                    df_merge[index]=df_merge[ind]

    return(df_merge)

def clean_compare(df_in,df_compare,lev):
    """ Requires one column dataframe to be cleaned and a comparison list
    lev is the tolerance of the levenshtein ratio between comparison list and input data
    """
    N,=df_in.shape
    for ind in range(N):
        if df_in[ind] in df_compare.values.tolist():
            pass
        else:
            for index, value in df_compare.items():
                if Levenshtein.ratio(df_in[ind],df_compare[index]) >= lev: #finds the best match
                    #print(df_in[ind],df_compare[index]) # checking it's working
                    df_in[ind]=df_compare[index]
    
    return(df_in)



class Error_check():
    
    """
	levenshtein_ratio_and_distance:
	Calculates levenshtein distance between two strings.
    """
    def __innit__():
        pass
    #using static method to reduce possible memory load within the instance
    def data_cleaner(df,df_airports):
        """ Returns clean data """
        #clean_data= unclean_data.dropna() ## gets rid of blank data rows. This maybe more efficent if we include it in the map reduce process...
        ## Using regex characters to make sure the correct formating of the data set is used.
        ## A format check is done on the data to only calculate the levenstein ratio on incorrect rows to save processing time
        # data is returned into individual columns to allow processing of objectives


        #### Cleaning passenger id data
        df_pass=df[0]
        df_compare_pass = df_pass.str.contains(r'^[A-Z]{3}\d{4}[A-Z]{2}\d') #comparing the correct format
        df_pass=clean_replace(df_pass,df_compare_pass,0.8, False) # cleaning the data using the Levenshtein ratio

        #### cleaning using Flist of top 30 airports

        df[2]=clean_compare(df[2],df_airports[1],0.6) # cleaning the data Levenshtein ratio and top 30 airports
        df[3]=clean_compare(df[3],df_airports[1],0.6) # cleaning the data Levenshtein ratio and top 30 airports
 
        #### cleaning flight id and merging dependent rows to imporove data cleaning accuracy
        df_flights=df[1]+'@,'+df[2]+'@,'+df[3] #using a unique character splitter
        df_compare_flights = df_flights.str.contains(r'^[A-Z]{3}\d{4}[A-Z]@,[A-Z]{3}@,[A-Z]{3}') #comparing the correct format
        df_flights=clean_replace(df_flights,df_compare_flights,0.8, False) # cleaning the data using the Levenshtein ratio
        
        #### Duplicate removal ####
        df_dup = df_pass+"<dup>"+df_flights+"<dup>"+df[4].astype(str)+"<dup>"+df[5].astype(str)
        df_dup=df_dup.drop_duplicates()
        df_dup=df_dup.dropna()
        df_dup=df_dup.reset_index(drop=True)
        df_dup = df_dup.apply(lambda x: pd.Series([i for i in x.split('<dup>')]))
        df_pass=df_dup[0]
        df_flights=df_dup[1]
        df_flights = df_flights.apply(lambda x: pd.Series([i for i in x.split('@,')]))#splitting the columns
        df[4]=df_dup[2].astype(int)
        df[5]=df_dup[3].astype(int)


        return(df_pass,df_flights,df[4],df[5])