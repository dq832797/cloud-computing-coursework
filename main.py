import pandas as pd
import numpy as np
import csv
import os
import sys
from error_check import Error_check as er # internal
from multiprocessing import Process, Queue
import time
from map_reducer import Map_reducer as mr # internal
from infographics import Infographics as inf #internal

#• obj1 Determine the number of flights from each airport; include a list of any airports not used.
#• obj2 Create a list of flights based on the Flight id, this output should include the passenger Id, relevant
#  IATA/FAA codes, the departure time, the arrival time (times to be converted to HH:MM:SS format),
#  and the flight times.
#• obj3 Calculate the number of passengers on each flight.
#• obj4 Calculate the line-of-sight (nautical) miles for each flight and the total travelled by each passenger
#  and thus output the passenger having earned the highest air miles.

start = time.perf_counter() #To provide a performance metric.

###########################################################################
# Function to determine the number of processors available for the mapper nodes
def get_threads():
    """ Returns the number of available processors on a posix/win based system """
    if sys.platform == 'win32':
        return (int(os.environ['NUMBER_OF_PROCESSORS']))
        
    else:
        return (int(os.popen('grep -c cores /proc/cpuinfo').read()))

if not os.path.exists('data_out'): #making sure the out directory exists
    os.makedirs('data_out')


####################################################################
##################### Getting Data #################################
### batch control need to modify with regards to memory allocation##
#this can also be tied into a dynamic server allocation. 

def file_getter(file_loc, file_name, ext):
    """Function to get file it also performs an error check to ensure file 
    exist as part of future debugging
    """
    loc_file=os.getcwd() + file_loc + file_name + ext ##add chunk size to limit the amount of incomming data
    if os.path.isfile(loc_file):
       df=pd.read_csv(loc_file, header=None) # no need to conduct a with statement here pd.read_csv automatically closes the file
       return(df)
    else:
       print("File does not exist!-Please ensure the file is in the data_in directory")
       sys.exit()
       


df_passenger=file_getter("/data_in/",r"AComp_Passenger_data", r".csv").dropna()
df_passenger=df_passenger.reset_index(drop=True)# Getting passenger data and removing blanks
df_airports=file_getter("/data_in/", r"Top30_airports_LatLong", r".csv").dropna() ## getting airport codes and removing blanks
df_airports=df_airports.reset_index(drop=True)



""" Data cleaner before mapreduce process not used
df_airports = er.data_cleaner(df_airports) #getting rid of the blank rows
df_passenger = er.data_cleaner(df_passenger) #getting rid of the blank rows

df_passenger=file_getter("/data_in/",r"AComp_Passenger_data_no_error", r".csv")
"""

######################## Split ####################################

df_mapper=np.array_split(df_passenger, get_threads()) ## splitting the input array (this is a numpy array)

######### Cleaning ###########
del df_passenger ## freeing memory no longer need df_passenger object after spliting

####################################################################
####################################################################



################# Mapper and local reducer #########################
####################### Processing #################################
map_workers=[]
for thread_id in range(get_threads()): ##### Initialising the processors
    p = Process(target=mr.run_mapper(df_airports,thread_id,df_airports,df_mapper[thread_id]), args=(thread_id,))
    p.start()
    map_workers.append(p)
[t.join() for t in map_workers] ## closing the threads

########## Cleaning ##########
del df_mapper ## freeing memory no longer need df_mapper object after the mapper phase

########################## Reducer ################################

to_sort_ob1,to_sort_ob2,to_sort_ob3,to_sort_ob4=mr.reducer_combiner(get_threads())

###################################################################

finish = time.perf_counter()

print(f'Finished in {round(finish-start, 2)} second(s)')

############ Creating a visualisation of the output ##############

inf.info_out(df_airports,to_sort_ob1,to_sort_ob2,to_sort_ob3,to_sort_ob4)
